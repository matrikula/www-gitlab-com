---
layout: markdown_page
title: Courses
description: Learn about GitLab courses, including course listings and information for adding and uploading courses.
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Deprecation

We are deprecating this page.
As noted in [our handbook guidlines](/handbook/handbook-usage/#guidelines) we want to organize per function and result, not per format.
All video's on this page should be embedded in the relevant part of the handbook.



### Sales (SLS) courses

 - SLS001 [How to Conduct an Executive Meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)

### Customer Success (CST) courses


- CST207 [Automate to Accelerate](https://www.youtube.com/watch?v=dvayJWwzfPY&t=465s)
- CST208 [Installing GitLab on GKE](https://www.youtube.com/watch?v=HLNNFS8b_aw&t=1s)
- CST209 [Connecting GitLab.com to your private cloud on GCP](https://www.youtube.com/watch?v=j6HRDquF0mQ)
- CST210 [Complete DevOps with GitLab](https://www.youtube.com/watch?v=68rGlAihKFw)
