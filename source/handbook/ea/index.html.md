---
layout: markdown_page
title: "Executive Assistants"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

This page details processes specific to the Executive Assisstants of GitLab. The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.
If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them. Many items on this page are a guidelines.

## Meeting request requirements

If you want to schedule a meeting, email the EA the following:
* Meeting type: internal prep, client facing/customer, prospective customer, etc
* Urgency: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Provide context: include topic, agenda to be covered. Share google doc if available to be included in invite

In case you have a same day request, you can send the same information in the "ea-team" Slack channel


### Formats for invites
* MTG for meetings in person, either at the "Experience Center" or another location
* INTERVIEW for interviews (make sure to loop in our PR partner)
* PHONECALL for phone calls
* VIDEOCALL for video conference calls using Zoom. Using the [Zoom plugin for Google Calendar](https://about.gitlab.com/handbook/communication/#video-calls) you can easily get the info you need in the invite.
    Example: “VIDEOCALL | Kirsten Abma for Executive Assistant”
* Make sure to remove the Hangout link first and then use the Zoom extention.
* Please add the subject of the call in the description, for internal and external calls.
* When meetings are being rescheduled please put RESCHEDULING at the beginning of the appointment
* When video calls or meetings are being recorded add RECORDING to the invite so people at the Experience Center can see it and keep the background noise minimal
* Addresses in calendar invites should only have an address and nothing else
* *Everyone external* receives a link to the [GitLab About page](https://about.gitlab.com/about). Only for final interviews people receive the form to fill out.
* *All* holds on Sid's calendar need a reason so he can judge when the hold might no longer apply.

Make sure to include in the description

### Videocalls

<br>
- Please read our About page as preparation for this meeting: https://about.gitlab.com/about/<br>
- [ONLY calls in the hiring process] Please fill out this form a day in advance, to discuss during the call: https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLScXUW07w36Ob2Y2XQuESBaYqU5_c1SoweGS1BzGHnbesISGXw/viewform<br>
<br>

### Meetings at the Experience Center

<br>
Please read our primer as preparation for the meeting: https://about.gitlab.com/primer<br>
CEO cell: <br>
Guest cell:<br>
Accessing the GitLab office building<br>
Enter the building through the double doors between the two Mike's Bikes locations.<br>
On the intercom press the call button then press '206' (for GitLab) to have us buzz you in.<br>
Proceed to the elevator at the rear of the foyer and go to the 2nd floor (strangely the elevator will show 3 when you stop at 2).<br>
Exit the elevator and proceed to the right. The unit is the last door on the right at the end of the hall (2F).<br>
More details are on our visiting page: https://about.gitlab.com/visiting/<br>
<br>


## General scheduling guidelines

* [everytimezone.com](http://www.everytimezone.com) can help determine the best time to schedule
* You can add other [calendars](calendar.google.com) on the left, to see when GitLab team members are free to schedule a meeting with
* The [agenda](https://docs.google.com/document/d/187Q355Q4IvrJ-uayVamoQmh0aXZ6eixAOE90jZspAY4/edit?ts=574610db&pli=1) of items to be handled by the EA for the CEO
* Use for example a tool like [Skyscanner](www.skyscanner.com) to find different flight options with most airlines when needing to book travel
<br>
<br>
* Schedule calls in European timezones in the morning (am) Pacific (Daylight) Time and US time zones in the afternoon (pm) Pacific (Daylight) Time
* Don't schedule over the Infrastucture team call unless approved by the CEO
* Keep 1 hour open in calendar per day for email
* Holds on the schedule should be removed at least 15 minutes before the preceeding meeting starts.
* After each meeting with a potential investor, make sure to update the sheet with the information on these meetings (to be found in the agenda doc for EAs)
* Monthly video calls are 25 minutes while quarterly calls/dinners are scheduled for 90 minutes plus any necessary travel time.
* If the call is with any Google company, use Hangouts instead of Zoom.
* Meetings at the Experience Center with another guest joining via videocall. The EA will schedule an additional ten minutes before the appointment to test the system.
<br>
<br>
* Monthly video calls are 25 minutes while quarterly calls/dinners are scheduled for 90 minutes plus any necessary travel time.
* Mark the events listed in [Google Calendar section](/handbook/communication/#google-calendar) of the handbook as 'Private'
* For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled.
* Make sure to plan travel time (in a separate calendar item, just for Sid) before and after the meeting in case another meeting or call should follow.
* If Sid has a **ride or walks** to an appointment, make sure to **add 5 minutes extra** to find the address and sign in at reception.
* If Sid is **driving himself**, make sure to **add 15 minutes extra** for random occurances such as travic, stopping for gas or parking.
* If Sid is **driving himself** to a meeting, he likes to plan phonecalls to catch up with the team. Check with him who he'd like to talk with during his commute and schedule accordingly.
* Sales meetings are important. If the CEO can help the process, feel free to include him in the meeting by working with the EA on this.
* Due to a busy schedule the CEO has a preference of meeting setup: First try for a video call or  a meeting at the GitLab Experience Center. If the other party presses to meet at their location, confirm if that is OK before accepting.

### Executive F2F Meetings and Board Meetings

There should be one invite for all attendees that includes the following:

* Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
* Zoom Link
* Agenda (the agenda should also include the zoom link at the top)

Make sure to:

* Test Zoom set-up at least 1 hour before and at most 2 days before
* Ensure remote participants feel invited and welcomed

## Pick your brain meetings

If people want advice on open source, remote work, or other things related to GitLab we'll consider that. If Sid approves of the request we suggest the following since we want to make sure the content is radiated as wide as possible.:

1. We send an email: "Thanks for being interested in GitLab. If we schedule a meeting it will follow the format on https://about.gitlab.com/handbook/ceo/#pick-your-brain-meetings Are you able to submit a draft post with us within 48 hours of interview?"
1. If we receive a positive answer we schedule a 50 minute Zoom video call or [visit to our Experience Center](https://about.gitlab.com/visiting/) that is recorded by us, uploaded to Youtube as a private video, and shared with you.
1. Within 48 hours you share a draft post with us in a Google Doc with suggestion or edit rights for anyone that knows the url.
1. You can redact anything you don't want to publish.
1. Our content department will work with you to publish the post within the agreed timeframe.
1. A great examples of this in action are the first two times we did this [https://about.gitlab.com/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/](https://news.ycombinator.com/item?id=12615723) and [https://news.ycombinator.com/item?id=12615723](https://news.ycombinator.com/item?id=12615723). Both got to nr. 1 on [Hacker News](https://news.ycombinator.com/).

The EA-team will create an issue once it's scheduled with the label [ceo-interview on GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com/issues?label_name=ceo-interview&scope=all&state=all) should follow up to make sure the draft post is submitted.

Reply to emails: Thanks for wanting to chat. I propose we meet in the format proposed on https://about.gitlab.com/handbook/ceo/#pick-your-brain-meetings so that other people benefit from our conversation too. If you're up for that please work with Kirsten (cc:) to schedule a time and location.

Alternatively we can also make it a YouTube live event, people can ask us questions in the chat during the event, and we can share the recording later.

## Email

* Labels: /archive, /respond or /urgent-important
* Prepare draft responses
* Standard reply for recruiters:
“We do not accept solicitations by recruiters, recruiting agencies, headhunters, and outsourcing organizations. Please find all info [on our jobs page](https://about.gitlab.com/jobs/#no-recruiters)
* Follow up on introductions from certain recipients (board, investors) immediately without asking for CEO approval.

## Physical Mail

* Check all incoming (physical) mail at 1233 Howard Street and sort the urgent and important letters.
* Inform AP if invoices came in.
* Inform other people/departments if mail is addressed for them and include a scan of the document.

## Travel

EA does research for the best option for a flight and propose this to the CEO before booking.
Make sure to add a calendar item for 2 hours before take off for check in and add a separate one for travel time before that in the CEO's calendar.
If a flight was changed or not taken due to delays or other circumstances, make sure to check with the airline for the persons flight status.
Current preferences for flights are:
* Aisle seat
* Check a bag for all trips longer than one night
* Frequent Flyer details of all (previously flown) airlines are in EA vault of 1Password as well as important passport/visa info

## Expensify

* When you’re logged in, you can find wingman account access for other team members in the top right corner menu.
* Check their email (if you have access), using the search bar in the top, to find any receipts for the postings in the current expense report.
* And/or write down what receipts are missing and email to request them if needed.
