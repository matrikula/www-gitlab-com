---
layout: markdown_page
title: "CEO"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

This page details processes specific to the CEO of GitLab.
The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.
If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them.
Many items on this page are a guidelines for our [Executive Assistants](https://about.gitlab.com/roles/people-ops/executive-assistant/) (EAs)

## Favorite places

* Favorite lunch place: [Basil Thai in SoMa](http://www.basilthai.com/home.html).
* Favorite dinner places: [Heirloom Cafe](https://heirloom-sf.com/), [Ozumo](http://www.ozumo.com/) (seated in the bar area), or a BBQ at our Experience Center.
* Favorite drinks place: [Odd Job](http://oddjobsf.com/).

## Flaws

Transparency and directness are part of our [values](https://about.gitlab.com/handbook/values) and I want to live them by sharing the flaws I know I have.
I'm fully responsible for improving the things below, listing them is no excuse.
They are listed here for two reasons.
The first one is so that people know it is not them but my fault.
The second one is so I can improve, I hope that listing them lets people know I appreciate when people speak up about them.

1. I look serious all the time, it is OK to say 'maybe you can smile more.'
1. I love debating, it is OK to say 'please stop debating and start collaborating' or 'we should have a [dialectic](https://en.wikipedia.org/wiki/Dialectic) instead of a debate.'
1. My English pronunciation, choice of words, and grammar are not great. I'm taking lessons but I welcome corrections when we're having a 1:1 conversation and/or when it might confuse people.
1. When in a rush I will jump to conclusions, it is OK to ask 'can we take more time to discuss this.'
1. I sometimes make reports feel like I'm scolding them, as in being angry for a perceived fault. It is OK to say, I don't mind you making that point but your tone doesn't make me feel respected.
1. In my feedback I sometimes sound more like I'm giving an order instead of offering a suggestion, even when I mean the latter. It is OK to say 'that sounds like an order, I would have appreciated it more in the form of a suggestion.'
1. I sometimes fail to distinguish which of the [three levels of performance](#three-levels-of-performance) I'm talking about. It is OK to ask 'is that a commitment, an aspiration, or a possibility?'.

If you speak up about them I should thank you for it, it is OK to say 'this was on your list of flaws so I kinda expected a thank you'.
I'm sure I have more flaws that affect my professional life.
Feel free to send a merge request to add them or communicate them anonymously to our Chief Culture Officer so she can send a merge request.

Not a flaw but something to know about me, I have [strong opinions weakly held](https://blog.codinghorror.com/strong-opinions-weakly-held/). Or as someone said, I come in hot but am open to new evidence.

## Communication

Thanks to [Mårten Mickos](https://www.linkedin.com/in/martenmickos) for the inspiration for this section. All good ideas are his, all bad ones are mine.

I am a visual person much more than auditory, and I am a top-down person much more than bottom-up. This means that I love written communication: issues, email, Google Docs, and chat. Feel free to send me as many emails and chat messages as you like, and about whatever topics you like.

If you have a great new idea or suggestion for me, I appreciate if you can convey it in a picture or in written words, because I learn by seeing more than I learn by hearing. I don't mind if you send me or point me to plans that are in draft mode or not ready. I am happy if I can give useful feedback early. It doesn’t have to be perfect and polished when presented to me.

In written communication, I appreciate the top-down approach. Set the subject header to something descriptive. Start the email by telling me what the email is about. Only then go into details. Don't mix separate topics in the same email, it is perfectly fine to send two emails at almost the same time. Try to have a concrete proposal so I can just reply with OK if that is possible.

I get many email on which I am only cc'd on, I would very much appreciate if you started emails intended specifically for me with "Sid," or some other salutation that makes it clear that the message is for me.

I have accounts on [LinkedIn](https://www.linkedin.com/in/sijbrandij) and [Facebook](https://www.facebook.com/sytse). I will not send invites to team members on those networks since as the CEO I don't want to impose myself on anyone. But I would love to connect and I will happily accept your LinkedIn and Facebook friend request. You can also find me on Twitter as [@sytses](https://twitter.com/sytses), I won't request to follow private twitter accounts, I assume I'm welcome to follow public twitter accounts, if not please let me know.

## Please chat me the subject line of emails

I get a lot of email and I'm frequently not on top of it.
I appreciate if you sent me a chat message if I need to respond to something.
Please quote the subject line of the email in your chat message.

## Meeting request requirements

For scheduling a video call or meeting with me, please see the [EA handbook page](gttps://about.gitlab.com/handbook/ea)


## Sending email

If someone else in the company wants to have me send an email they should email me and my EA with:

1. Instruction: "Please email this, please bcc: me on the outgoing email, forward any responses, and cc: me on an further emails."
1. Recipient name
1. Recipient email
1. Email subject
1. Email body

When receiving such an email my EA should stage a draft email to the recipient and a draft answer 'done'.

## Hiring process

* There are 2 types of calls with the CEO. First an introductory call which needs the [GitLab primer](https://about.gitlab.com/primer) and second a final interview which needs the form mentioned below.
* PeopleOps makes sure that every candidate that the CEO talks with has a profile in our ATS with background info e.g. a resume or LinkedIn profile link.
* People Ops will tag EA to schedule a call for the candidate with Sid.
* Template email with wording to schedule “Kirsten schedule Sid.”
* Add date/time and make sure to include timezones.
* For calls with candidates for a role of Director level or higher, make sure to plan 90 minutes for a call.
* Add responses of the [form](https://docs.google.com/a/gitlab.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) you sent out with the invite to our ATS; [Lever](hire.lever.co/candidates).


## Sales meetings

* I love to talk to users or potential users of GitLab anytime.
* Traveling is not efficient because it can take 2 to 10 times the time of the meeting itself.

Some general guidelines of what travel is appropriate, these guidelines are not fixed, feel free to ask for exceptions:

1. Check my availability with the EA, reschedule with the EA, cancel with the EA, not me.
1. I'll take any meeting via video conference or at our Experience Center.
1. I'll take a meeting in the Bay Area as long as it is not an SMB organization.
1. I'll take a meeting outside the Bay Area but in the US with large or strategic organizations
1. I'll take a meeting outside the US with strategic organizations.

Consider the following to increase efficiency:

- Combine meetings with multiple organizations in the same location.
- Get meetings with multiple stakeholders in the same company.
- Apart from the formal meetings try to organize a meal with stakeholders.
- Record the meeting so you can distribute it to others in the organization.
- Please check with the EA to look at my calendar to leverage my existing travel plans.
- Make sure to let the EA know what you expect from me. E.g. arrive an hour before, do a pitch etc.
- Please plan audio only meetings only when the customer explicitly declines a video call.
- Any meeting that requires travel should be properly prepared and any lessons relayed to the marketing team.

## Conferences

When at conferences I want to achieve results for the company and be efficient with my time.
Please ask sales and/or marketing to set up meetings for me in advance.
I don't mind doing booth duty, presenting, or any other way I can contribute.
I do mind unscheduled time randomly wandering the hallways, I've found this to be ineffective.

Each year I want to attend the open source leadership summit organized by the Linux foundation.
Please ensure:

1. We submit at least one talk.
1. We book the on-site hotel on the day it opens up.
1. We let the sales team know we would love to set up meetings.
1. There is same timezone EA coverage during the conference.

## House

If you are a GitLab team member you can use our house in Utrecht, the Netherlands for free with up to 5 guests.
You can find more information [on AirBnB](https://www.airbnb.com/rooms/22758267).
When it is not reserved on the AirBnB calendar you can reserve it, message Karen Sijbrandij in the #valley chat channel to do so.
Our host Justus will check you in and out and will take care of the cleaning.
Enjoy your stay!

## Three levels of performance

There are three levels of performance:

1. Commitment: what we promise to our stakeholders.
1. Aspiration: want to get to 70% of this, these are our [OKRs](https://about.gitlab.com/okrs/)
1. Possibility: what are intrigued by, inspired by, and what I talk about

I'm driven by what is possible, the aspiration, what can be.
What is possible is more than what we are satisfied with or what we promised to our stakeholders.
We can be above what we promised and below what is possible and still have done a good job, we can win without doing everything we aspired to do, or everything that is possible.
It is unlikely that we win without doing what we promised.
I have to be clear in distinguishing these level when I discuss a goal with my reports.
