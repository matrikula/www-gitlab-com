---
layout: markdown_page
title: "Distribution Team Triage"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common links

* [Engineering Team Triage](https://about.gitlab.com/handbook/engineering/issue-triage/)

## Triaging Issues

Triaging issues involves investigating and applying labels and milestones to issues.

[Issues filter](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=0&milestone_title=No+Milestone&scope=all&sort=created_date&state=opened)

Issues that need triaging are considered as follows:
  - They have No Milestone
  - They have No Author assigned
  - They do **not** have the following labels applied:
    * awaiting feedback
    * for scheduling

Issues are considered **partially** triaged if they have been assigned `for scheduling` or `awaiting feedback` labels.

Issues are **fully** triaged when they have been assigned a Milestone, even if it is `Backlog`

An issue that has been assigned to a user, but has no milestone, is not triaged, but is considered the responsibility of that user, and is not part of our triage queue at this time.

## Label glossary

| Label | What it means | How to handle it |
| - | - | - |
| Awaiting Feedback | Information has been requested from the user | If no reply has been received in two weeks, the issue can be closed. |

## Response templates

Copy and paste into issues where appropriate

#### For problems not related to package installation and configuration

If someone is asking for support in the omnibus-gitlab project, point them to the correct place to look

```
This doesn't appear to be an issue with omnibus-gitlab itself. This can be the case when installation and `gitlab-ctl reconfigure` run went without issues, but your GitLab instance is still giving 500 error page with an error in the log.

You can check for ways to get help at the [GitLab website](https://about.gitlab.com/getting-help/).

/close
```
